import requests
import json
data = requests.get('https://restcountries.eu/rest/v2/all')
api=data.json()
country=str(input("enter a country do u want to check its details :"))
country=country.title()
print(country)
for f in api:
    if(f['name']==country):
        print('capital of this {} is '.format(country)+(f['capital']))
        print("Currency of {} is :{} and Symbol is : {} ".format(country,f['currencies'][0]['name'],f['currencies'][0]['symbol'] ))
        print("domain of {} is:{}".format(country,f['topLevelDomain']))
        for i in f['languages']:
            print("language spoken in {} is {} and nativeName of this language is {}".format(country,i['name'],i["nativeName"]))
        print("callingCode of {} is: +{}".format(country,f['callingCodes']))
        print("population of {} is:{}".format(country,f['population']))
        for i in f['regionalBlocs']:
            print(i['acronym'])
        print("Region of {} is:{}".format(country,f['region']))
        print("subregion of {} is:{}".format(country,f['subregion']))
        print("latitude  of {} is:{}".format(country,f['latlng'][0]))
        print("longitude  of {} is:{}".format(country,f['latlng'][1]))
        for i in f['timezones']:
            print("timezone of {} is:{}".format(country,i,))
        for i in f['borders']:
            print("this nation shared border with this countries :{}".format(i,end="."))
        print("nativeName of {} is:{}".format(country,f['nativeName']))